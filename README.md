# MovieClassifierTool
The aim of the Tool is to classify the films starting from the Title-Description.

# Running On Your Local Computer
## Required software:
* python3
* facultative (pip install -r requirements.txt)
* requirements will be checked during the running of the movie_classifier_prediction.py
# Once Prerequisites Are Installed
Now that the prerequisites are ready to go it's a few simple commands to get your instance up and running.

# Get the latest version of MovieClassifierTool
git clone https://nicoladrianopallottino@bitbucket.org/nicoladrianopallottino/movie_classifier_tool.git

# Enter the movie_classifier_tool folder that was just created
cd movie_classifier_tool

# Run the program following application arguments:
python3 movie_classifier_prediction.py --title <title> --description <description>


* <title> : string format (example: "Othello")
* <description> : string format (example: "The story about..")

# Docker image
* see docker_instructions.txt

# Report Training
* see movie_classifier_training.html

# Library
* go in utility and check utility.py