FROM python:3.6.4-slim-jessie
ADD . /
WORKDIR /
RUN pip install -r requirements.txt
ENTRYPOINT ["python3"]
CMD ["movie_classifier_prediction.py","--title","her","--description","Story of a Ghost into the nightmare with a boy in which the killed mum"]
