#utf-8
import pandas as pd
import numpy as np
import os
import ast
import gc
from collections import Counter
import pickle
from tqdm import tqdm_notebook as tqdm

#nltk
import nltk
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords, wordnet
#sklearn
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import TruncatedSVD
from sklearn.random_projection import sparse_random_matrix
from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer,TfidfVectorizer
from sklearn.preprocessing import StandardScaler,MinMaxScaler,LabelEncoder
from sklearn.model_selection import train_test_split,GridSearchCV, RandomizedSearchCV, StratifiedKFold
from sklearn.metrics import (pairwise_distances, recall_score,accuracy_score, confusion_matrix,
                             f1_score, precision_score, auc, classification_report,
                             roc_auc_score,roc_curve, precision_recall_curve)

#imblearn (only for training)
from imblearn.over_sampling import SMOTE, RandomOverSampler
from imblearn.under_sampling import NearMiss, RandomUnderSampler
from imblearn.combine import SMOTEENN,SMOTETomek

def tokenizer_porter(porter, text):
    """
    Parameters:
    porter : func
    text : list
    """
    return[porter.stem(word) for word in text]

def tokenizer_lem(lemm, text):
    """
    Parameters:
    lemm : func
    text : list
    """
    return[lemm.lemmatize(word) for word in text]

def get_weight(count, eps=1, min_count=2):
    """
    Parameters:
    count : float
    eps : int
    min_count : int

    Simple Func to get weights
    of the words exactly in each part of
    the sencences.
    """
    if count < min_count:
        return 0
    else:
        return 1 / (count + eps)
   
def cosine_distance(u, v):
    """
    Parameters:
    u : matrix
    v : matrix
    
    Returns the cosine of the angle between
    vectors v and u. This is equal to
    u.v / |u||v|.
    Experimental phase, not used here.
    """
    return np.dot(u, v) / (math.sqrt(np.dot(u, u)) * math.sqrt(np.dot(v, v))) 

def flat(aggregate_lists, mapping=False):
    """
    Parameters:
    aggregate_lists : [[]]
    mapping : bool

    Description:
    Returns Flattern
    list of values
    by list of lists
    example input:
    flat([[1,2],[3,4]])
    example output:
    out: [1, 2, 3, 4]
    """
    return [vectors for vector in aggregate_lists for vectors in vector if not mapping or vectors in mapping]

def wiPCA(X, energy_ratio=0.99, component = 2,
          norm=True, truncated=True, truncate_iter=5):
    """
    PCA with truncated SVD,
    assume features are column oriented.
    Experimental phase, not used here
    due to limit of computation.
    """
    if truncated:
        print('truncated svd')
        svd = TruncatedSVD(n_components=component,
                           n_iter=truncate_iter) 
        svd.fit(X)
        idx = np.argsort(-svd.explained_variance_)
        s = svd.explained_variance_[idx]
        v = svd.components_[idx, :]
    else:
        print("reduced rank svd")
        vmean = np.mean(X, axis=0) 
        X = X - vmean
        X = X * (1/np.sqrt(X.shape[0]))
        u, s, v = np.linalg.svd(X, full_matrices=False)
        s = s ** 2

    v = v[np.where(np.cumsum(s) <= energy_ratio * sum(s)
                  )[0], :]
    pX = np.dot(X, v.T)
    
    if norm:
        pXmean = np.mean(np.abs(pX), axis=0)
        pX = pX / pXmean
        
    return pX, v, s

def transform(transformer, X, y):
    """
    Parameters:
    transformer : func
    X : matrix
    y : array

    Function that returns X_resempled
    & y_resempled depending on transformer passed
    into.
    Example transformer: RandomOverSampler
    """
    print("Transforming {}".format(transformer.__class__.__name__))
    X_resampled, y_resampled = transformer.fit_sample(
        X.values, y.values.ravel())
    return (transformer.__class__.__name__, pd.DataFrame(
        X_resampled), pd.DataFrame(y_resampled))

def overbalance_target_df(X_train, y_train, target,
                          majority_class, frac,
                          random_state):
    """
    Parameters:
    X_train : matrix
    y_train : array
    target : str
    majority_class : str/int
    frac : float
    random_state : int

    Function that returns X_train
    & y_train minority classes rebalanced
    based on the fraction passed
    into.
    """
    from sklearn.utils import resample

    X_train = X_train.merge(y_train,
                            'left',
                            left_index=True,
                            right_index=True)
    df_majority = X_train[X_train[target] == majority_class]
    df_minority = X_train[X_train[target] != majority_class]
    minor_classes = []
    classes = [c for c in list(y_train[
        y_train.columns[0]
    ].value_counts().index)]
    classes.remove(majority_class)
    for tgt in classes:
        df_minor_downsampled = resample(
            df_minority[df_minority[target] == tgt],
            replace=True,
            # sample with replacement
            n_samples=int(df_minority[
                df_minority[target] == tgt].shape[0]+
                df_minority[
                    df_minority[target] == tgt].shape[0]*frac),
                    random_state=random_state)
        minor_classes.append(df_minor_downsampled)
    # reproducible results with random_state
    # Combine minority classes with downsampled majority class
    X_train_balanced = pd.concat(
        [pd.concat(minor_classes), df_majority])
    X_train.pop(target)
    y_train_rebalanced = X_train_balanced[[target]]
    X_train_balanced.pop(target)
    del df_majority,df_minority,df_minor_downsampled
    return X_train_balanced, y_train_rebalanced
