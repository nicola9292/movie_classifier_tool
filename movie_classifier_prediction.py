#!/usr/bin/python3
# coding: utf-8

import argparse
import subprocess
import sys
import pkg_resources

parser = argparse.ArgumentParser(description='Movie Classifier')
parser.add_argument(
    '--title','--title',
     help='Description for title argument', required=True)
parser.add_argument(
    '--description','--description',
     help='Description for title argument', required=True)
args = parser.parse_args()

if len(args.description) <= 10:
    raise Exception("Warning! no description found: more informations required!")

def install(package):
    subprocess.check_call([sys.executable,
     "-m", "pip", "install", package])

with open('requirements.txt', 'r') as f:
    f = f.read()

try:
    pkg_resources.require(f.rsplit('\n'))
except:
    print('Warning! Missed some requirements!')
    for package in f.rsplit('\n'):
        install(package)

maps_output = {
"Action"        :  0,
"Adventure"     :  1,     
"Animation"     :  2,     
"Comedy"        :  3,     
"Crime"         :  4,     
"Documentary"   :  5,     
"Drama"         :  6,     
"Fantasy"       :  7,     
"Foreign"       :  8,     
"History"       :  9,     
"Horror"        :  10,    
"Music"         :  11,    
"Mystery"       :  12,    
"Romance"       :  13,    
"ScienceFiction":  14,
"Thriller"      :  15,    
"War"           :  16,    
"Western"       :  17,    
}

from utility import utility as ut

backup_files = "./backup_files/"

with open(backup_files+'weighted_words.pkl', 'rb') as fp:
    weighted_worlds = ut.pickle.load(fp)
    
with open(backup_files+'vectorizer.pkl', 'rb') as fp:
    vectorizer = ut.pickle.load(fp)

with open(backup_files+'tfidf.pkl', 'rb') as fp:
    tfidf = ut.pickle.load(fp)
    
with open(backup_files+'svd.pkl', 'rb') as fp:
    svd = ut.pickle.load(fp)
    
with open(backup_files+'scaler.pkl', 'rb') as fp:
    scaler = ut.pickle.load(fp)

with open(backup_files+'model_lg.pkl', 'rb') as fp:
    model = ut.pickle.load(fp)

ut.nltk.download("wordnet", quiet=True)
ut.nltk.download("stopwords", quiet=True)
stops = set(ut.stopwords.words("english"))
stop_words = []
for item in stops:
    stop_words.append(item)

story = args.description
test = ut.pd.DataFrame(data=[story],index=[args.title],
                      columns=['overview'])

tokenizer = ut.RegexpTokenizer(r'\w+')
test.overview = test.overview.astype(str)
test['overview'] = test.overview.apply(
    lambda x: ' '.join(tokenizer.tokenize(x)))
test['lenght'] = test.overview.apply(len)
test['to_preprocess'] = test.overview.apply(
    lambda x: [c.lower() for c in x.split(
        " ") if c.lower() not in stop_words and c != ''])
test['n_keywords'] = test.to_preprocess.apply(len)


porter = ut.PorterStemmer()
test['weighted_worlds_features'] = [[weighted_worlds[c.lower().split()[0]
                       ] for c in test.to_preprocess.iloc[
    0] if c in weighted_worlds.keys()]]

test['tokenizer_stemming_sentences'] = test.to_preprocess.apply(
    lambda x: ' '.join(ut.tokenizer_porter(porter, x)))

test.tokenizer_stemming_sentences = test.tokenizer_stemming_sentences.str.lower()

test['global_weight_of_words'] = test.weighted_worlds_features.apply(
    lambda x: ut.np.prod([c for c in x if c!= 0.0]))

preprocessed_text = ut.np.array(test.tokenizer_stemming_sentences)

bag = vectorizer.transform(preprocessed_text)

bag_trasformed = tfidf.transform(bag)
del bag

#polarity
positive = [] 
negative = []
positive1 = [] 
negative1 = []
positive2 = [] 
negative2 = []
  
for syn in ut.wordnet.synsets("good"): 
    for l in syn.lemmas(): 
        positive.append(l.name()) 
        if l.antonyms(): 
            negative.append(l.antonyms()[0].name())

for syn in ut.wordnet.synsets("love"): 
    for l in syn.lemmas(): 
        positive1.append(l.name()) 
        if l.antonyms(): 
            negative1.append(l.antonyms()[0].name())
            
for syn in ut.wordnet.synsets("young"): 
    for l in syn.lemmas(): 
        positive2.append(l.name()) 
        if l.antonyms(): 
            negative2.append(l.antonyms()[0].name())
            


flag_schema = [
    ['FLAG_good', positive, []],
    ['FLAG_bad', negative, []],
    ['FLAG_love', positive1, []],
    ['FLAG_hate', negative1, []],
    ['FLAG_young', positive2, []],
    ['FLAG_aged', negative2, []],
]
fields = ['overview']

for main_keyword, sub_keywords, negative_flags in flag_schema: 
    main_flag = 'FLAG_' + main_keyword
    test[main_flag] = False 
    for field in fields:
        for sub_keyword in sub_keywords:
            sub_flag = 'FLAG_' + sub_keyword + '_' + field 
            test[sub_flag] = test[field].apply(
                lambda s: sub_keyword in str(s).lower()) 
            test[main_flag] = test[main_flag] | test[sub_flag] 
            test = test.drop(sub_flag, axis=1)
    for negative_flag in negative_flags: 
        test[main_flag] = test[main_flag] & (~test[negative_flag])
        
preprocessed_text = svd.transform(bag_trasformed)
weighted_words_features = test.weighted_worlds_features.values[0]
#len([c for c in total.columns if 'w_' in c]
if len(test.weighted_worlds_features.values[0])<148:
    weighted_words_features.extend(ut.np.zeros(
        148-len(test.weighted_worlds_features.values[0]))
        )
else:
    weighted_words_features = weighted_words_features[:148]
test = test.join(ut.pd.DataFrame(
    test.weighted_worlds_features.tolist(),
    index=test.index).add_prefix('w_'))

test = test.merge(ut.pd.DataFrame(preprocessed_text,columns=[
    'c_'+str(c) for c in range(preprocessed_text.shape[1])],
                                  index=[args.title]),
                   left_index=True,
                    right_index=True)

test = test[[c for c in test.columns if 'w_' in c or
 'c_' in c or
 'lenght' in c or
 'n_keywords' in c or
 'FLAG' in c or
  'global_weight_of_words' in c]].fillna(0)

test_scaled = ut.pd.DataFrame(
    scaler.transform(test),
    columns=test.columns)

print({
    "title": args.title,
    "description": args.description,
    "genre": (list(maps_output.keys())[
    list(maps_output.values()).index(
        model.predict(test_scaled.values)[0])])
})

